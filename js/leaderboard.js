function createTableRows(lBdata) {
  var table = document.querySelector("#leaderboard");

  for (var k = 0; k < lBdata.length; k++) {
    var tr = document.createElement("tr"),
        tdArr = [],
        dataArr = [`${k + 1}`, lBdata[k].nickname, lBdata[k].mode, `${lBdata[k].score}`];

    for (var i = 0; i < 4; i++) {
      tdArr[i] = document.createElement("td");
      data = document.createTextNode(dataArr[i]);
      tdArr[i].appendChild(data);
      tr.appendChild(tdArr[i]);
    }

    table.appendChild(tr);
  }
}

getLeaderboard();
