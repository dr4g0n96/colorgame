const cors = require('cors'),
      express = require('express'),
      mysql = require('mysql'),
      bodyParser = require('body-parser'),
      port = 3000,
      app = express(),
      connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'colorgame'
      });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.listen(process.env.PORT || port, process.env.IP);

app.get('/leaderboard', (req, res) => {
  let query = 'SELECT * FROM ?? ORDER BY ??.?? DESC',
      table = ['leaderboard', 'leaderboard', 'score'];
  query = mysql.format(query,table);
  connection.query(query, (err, rows) => {
    res.json(rows);
  });
});

app.post('/save-score', (req, res) => {
  let query = 'INSERT INTO ??(??,??,??) VALUES (?,?,?)',
      inserts = ['leaderboard', 'nickname', 'mode', 'score',
                 req.body.nickname, req.body.mode, req.body.score];
  query = mysql.format(query, inserts);
  connection.query(query, (err, rows) => {
    err ? res.json({ 'Error': true, 'Message': 'Error Executing MySQL query' }) :
          res.json({ 'Error': false, 'Message': 'Row Aded!'});
  });
});

console.log(`App listening on port ${port}`);
