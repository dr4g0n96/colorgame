const API = 'http://localhost:3000/',
      oldLocation = location.href;

function postResult() {
  let payload = {
    nickname: input.value,
    mode: mode,
    score: score
  };

  fetch(`${API}save-score`, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: 'post',
    body: JSON.stringify(payload)
  })
  .then(res => res.json())
  .then(data => {
    location.href = `${oldLocation}leaderboard/`;
  })
}

function getLeaderboard() {
  fetch(`${API}leaderboard`)
    .then(res => res.json())
    .then(data => createTableRows(data)) ;
}
