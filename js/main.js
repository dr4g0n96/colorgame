var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
var nrSquares = 6;
var health = 3;
var maxWidth = "1000px";
var widthPadding = "24%";
var score;
var winTimer;
var colors = [];
var mode = "HEX";
var pickedColor;
var squares = document.querySelectorAll(".square");
var colorDisplay = document.querySelector("#colorDisplay");
var scoreDispaly = document.querySelector("#scoreDispaly");
var healthImg = document.querySelectorAll(".life img");
var message = document.querySelector("#message");
var header = document.querySelector("header");
var resetButton = document.querySelector(".reset");
var modeButtons = document.querySelectorAll(".mode");
var modal = document.querySelector("#gameOver");
var helpModal = document.querySelector("#gameRules");
var helpBtn = document.querySelector("#help");
var instruction = document.querySelector(".instruction-container");
var closeBtn = document.querySelector("#close");
var container = document.querySelector("#container");
var form = document.querySelector(".form");
var label = document.querySelector("label");
var input = document.querySelector("input");
var finalScore = document.querySelector("#finalScore");
var displayMode = document.querySelector("#displayMode");
var saveBtn = document.querySelector(".save");

init();
function init() {
  reset();
  setUpModeButton();
  setUpResetButtons();
  setUpHelpButton();
  setUpSaveBtn();
  setUpSquares();
}

function setUpSquares() {
  for (var i = 0; i < squares.length; i++) {
    squares[i].addEventListener("click", e => {
      var clickedColor = mode === "HEX" ?
        rgb2hex(e.currentTarget.style.backgroundColor) :
        e.currentTarget.style.backgroundColor;
      if(clickedColor === pickedColor) {
        score++;
        scoreDispaly.textContent = score.toString();
        message.textContent = "Correct!";
        header.style.backgroundColor = clickedColor;
        changeColors(clickedColor);
        winTimer = setTimeout(() => {
          squaresReset();
        }, 1000);
      } else {
        health--;
        if(health === 0){
          lost();
        }
        manageHealth();
        e.currentTarget.style.backgroundColor = "#232323";
        message.textContent = "TRY AGANE!";
      }
    });
  }
}

function handleLevel() {
  switch (score) {
    case 0:
      nrSquares = 6;
      squareStyling();
      break;
    case 5:
    case 10:
    case 15:
    case 20:
    case 25:
    case 30:
      nrSquares += 3;
      squareStyling();
      health < 3 && health++;
      manageHealth();
      break;
    case 35:
      nrSquares = 28;
      health < 3 && health++;
      manageHealth();
      break;
    default:
      break;
  }
}

function squareStyling() {
  if(nrSquares === 6) {
    widthPadding = "24%";
    maxWidth = "1000px";
  } else if(nrSquares === 12) {
    widthPadding = "23%";
  } else if(nrSquares === 15) {
    widthPadding = "18%";
    maxWidth = "1200px";
  } else if(nrSquares === 18) {
    widthPadding = "15%";
    maxWidth = "1350px";
  } else if(nrSquares === 21){
    widthPadding = "13%";
  }
  container.style.maxWidth = maxWidth;
  for (var i = 0; i < squares.length; i++) {
    squares[i].style.width = widthPadding;
    squares[i].style.paddingBottom = widthPadding;
  }
}

function manageHealth() {
  for (var i = 0; i < healthImg.length; i++) {
    healthImg[i].src = health <= i ?
      `${location.href}lostHealth.png` :
      `${location.href}healthy.png`
  }
}

function lost(){
  //display modal
  modal.style.display = "flex";
  finalScore.textContent = score.toString();
  displayMode.textContent = mode;
  form.style.backgroundColor = pickedColor;
  label.style.backgroundColor = pickedColor;
  input.style.backgroundColor = pickedColor;
}

function setUpSaveBtn() {
  saveBtn.addEventListener('click', e => {
    postResult();
  });
}

function setUpModeButton() {
  for (var i = 0; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener('click', e => {
      _removeClass();
      e.currentTarget.classList.add("selected");
      e.currentTarget.textContent === "HEX" ?
        mode = "HEX" :
        mode = "RGB";
      reset();
    });
  }
}

function _removeClass(){
  for (var i = 0; i < modeButtons.length; i++) {
    modeButtons[i].classList.remove("selected");
  }
}

function reset() {
  modal.style.display = "none";
  score = 0;
  squaresReset();
  scoreDispaly.textContent = score.toString();
  health = 3;
  manageHealth();
}

function squaresReset(){
  handleLevel();
  clearTimeout(winTimer);
  colors = mode === "HEX" ?
    generateRandomHex(nrSquares) :
    generateRandomRGB(nrSquares);
  pickedColor = pickColor();
  colorDisplay.textContent = pickedColor;
  message.textContent = '';
  header.style.backgroundColor = "steelblue";
  for (var i = 0; i < squares.length; i++) {
    colors[i] ? (
      squares[i].style.display = "block",
      squares[i].style.backgroundColor = colors[i]
    ) : (squares[i].style.display = "none");
  }
}

function setUpResetButtons() {
  resetButton.addEventListener("click", () => {
    reset();
  });
}

function setUpHelpButton(){
  helpBtn.addEventListener("click", () => {
    helpModal.style.display = "flex";
  });
  closeBtn.addEventListener("click", () => {
    helpModal.style.display = "none";
  });
}

function changeColors(color) {
  for(var i = 0; i < squares.length; i++) {
    squares[i].style.backgroundColor = color;
  }
}

function pickColor(){
  var random = Math.floor(Math.random() * colors.length);
  return colors[random];
}

function generateRandomRGB(num) {
  let arr = [],
      randomColor;
  for (var i = 0; i < num; i++) {
    randomColor = randomRGB();
    randomColor !== "rgb(35, 35, 35)" ?
      arr.push(randomColor) :
      i--;
  }
  return arr;
}

function generateRandomHex(num) {
  let arr = [],
      randomColor;
  for (var i = 0; i < num; i++) {
    randomColor = randomHex();
    randomColor !== "#232323" ?
      arr.push(randomColor) :
      i--;
  }
  return arr;
}

function randomRGB() {
  let nrArr = [];
  for(var i = 0; i < 3; i++) {
    nrArr.push(Math.floor(Math.random() * 255));
  }
  return `rgb(${nrArr[0]}, ${nrArr[1]}, ${nrArr[2]})`;
}

function randomHex() {
  let hexColorString = '#';
  for (var i = 0; i < 6; i++) {
    hexColorString += hexDigits[Math.floor(Math.random() * hexDigits.length)];
  }
  return hexColorString;
}

// *****************************************************************************
//Function to convert rgb color to hex format
function rgb2hex(rgb) {
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
  return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}
